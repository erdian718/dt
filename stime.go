// Package stime provides some simplified (no time zone) time processing functions.
package stime

const layoutDateTime = "2006-01-02T15:04:05"
