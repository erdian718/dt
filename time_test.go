package stime_test

import (
	"testing"
	"time"

	"gitlab.com/erdian718/stime"
)

func TestToTime(t *testing.T) {
	now := time.Now()
	value := stime.ToTime(now)
	if !equalTime(now, value) {
		t.Fail()
	}
}

func TestNewTime(t *testing.T) {
	now := time.Now()
	value := stime.NewTime(now.Hour(), now.Minute(), now.Second())
	if !equalTime(now, value) {
		t.Fail()
	}
}

func TestNowTime(t *testing.T) {
	now := time.Now()
	value := stime.NowTime()
	if !equalTime(now, value) {
		t.Fail()
	}
}

func TestParseTime(t *testing.T) {
	now := time.Now()
	value, err := stime.ParseTime(time.TimeOnly, now.Format(time.TimeOnly))
	if err != nil {
		t.Fatal(err)
	}
	if !equalTime(now, value) {
		t.Fail()
	}
}

func TestTimeUTC(t *testing.T) {
	now := time.Now()
	value := stime.ToTime(now)
	if !time.Date(0, 0, 0, now.Hour(), now.Minute(), now.Second(), 0, time.UTC).Equal(value.UTC()) {
		t.Fail()
	}
}

func TestTimeLocal(t *testing.T) {
	now := time.Now()
	value := stime.ToTime(now)
	if !time.Date(0, 0, 0, now.Hour(), now.Minute(), now.Second(), 0, time.Local).Equal(value.Local()) {
		t.Fail()
	}
}

func TestTimeSub(t *testing.T) {
	now := time.Now()
	dur := time.Hour + time.Minute + time.Second
	value := stime.ToTime(now)
	if value.Add(dur).Sub(value) != dur {
		t.Fail()
	}
}

func TestTimeAdd(t *testing.T) {
	now := time.Now()
	dur := time.Hour + time.Minute + time.Second
	value := stime.ToTime(now)
	if !equalTime(now.Add(dur), value.Add(dur)) {
		t.Fail()
	}
}

func TestTimeAddWith(t *testing.T) {
	now := time.Now()
	value := stime.ToTime(now)
	if !equalTime(now.Add(time.Hour+time.Minute+time.Second), value.AddWith(1, 1, 1)) {
		t.Fail()
	}
}

func TestTimeFormat(t *testing.T) {
	now := time.Now()
	value := stime.ToTime(now)
	if value.Format(time.TimeOnly) != now.Format(time.TimeOnly) {
		t.Fail()
	}
}

func TestTimeString(t *testing.T) {
	now := time.Now()
	value := stime.ToTime(now)
	if value.String() != now.Format(time.TimeOnly) {
		t.Fail()
	}
}

func TestTimeBinary(t *testing.T) {
	value1 := stime.NowTime()
	data, err := value1.MarshalBinary()
	if err != nil {
		t.Fatal(err)
	}

	var value2 stime.Time
	if err := value2.UnmarshalBinary(data); err != nil {
		t.Fatal(err)
	}

	if value1 != value2 {
		t.Fail()
	}
}

func TestTimeText(t *testing.T) {
	value1 := stime.NowTime()
	data, err := value1.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var value2 stime.Time
	if err := value2.UnmarshalText(data); err != nil {
		t.Fatal(err)
	}

	if value1 != value2 {
		t.Fail()
	}
}

func equalTime(t time.Time, v stime.Time) bool {
	return t.Hour() == v.Hour() && t.Minute() == v.Minute() && t.Second() == v.Second()
}
