package stime_test

import (
	"testing"
	"time"

	"gitlab.com/erdian718/stime"
)

func TestToDate(t *testing.T) {
	now := time.Now()
	value := stime.ToDate(now)
	if !equalDate(now, value) {
		t.Fail()
	}
}

func TestNewDate(t *testing.T) {
	now := time.Now()
	value := stime.NewDate(now.Year(), now.Month(), now.Day())
	if !equalDate(now, value) {
		t.Fail()
	}
}

func TestNowDate(t *testing.T) {
	now := time.Now()
	value := stime.NowDate()
	if !equalDate(now, value) {
		t.Fail()
	}
}

func TestParseDate(t *testing.T) {
	now := time.Now()
	value, err := stime.ParseDate(time.DateOnly, now.Format(time.DateOnly))
	if err != nil {
		t.Fatal(err)
	}
	if !equalDate(now, value) {
		t.Fail()
	}
}

func TestDateUTC(t *testing.T) {
	now := time.Now()
	value := stime.ToDate(now)
	if !time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC).Equal(value.UTC()) {
		t.Fail()
	}
}

func TestDateLocal(t *testing.T) {
	now := time.Now()
	value := stime.ToDate(now)
	if !time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local).Equal(value.Local()) {
		t.Fail()
	}
}

func TestDateSub(t *testing.T) {
	now := time.Now()
	dur := 24 * time.Hour
	value := stime.ToDate(now)
	if value.Add(dur).Sub(value) != dur {
		t.Fail()
	}
}

func TestDateAdd(t *testing.T) {
	now := time.Now()
	dur := 24 * time.Hour
	value := stime.ToDate(now)
	if !equalDate(now.Add(dur), value.Add(dur)) {
		t.Fail()
	}
}

func TestDateAddWith(t *testing.T) {
	now := time.Now()
	value := stime.ToDate(now)
	if !equalDate(now.AddDate(1, 1, 1), value.AddWith(1, 1, 1)) {
		t.Fail()
	}
}

func TestDateFormat(t *testing.T) {
	now := time.Now()
	value := stime.ToDate(now)
	if value.Format(time.DateOnly) != now.Format(time.DateOnly) {
		t.Fail()
	}
}

func TestDateString(t *testing.T) {
	now := time.Now()
	value := stime.ToDate(now)
	if value.String() != now.Format(time.DateOnly) {
		t.Fail()
	}
}

func TestDateBinary(t *testing.T) {
	value1 := stime.NowDate()
	data, err := value1.MarshalBinary()
	if err != nil {
		t.Fatal(err)
	}

	var value2 stime.Date
	if err := value2.UnmarshalBinary(data); err != nil {
		t.Fatal(err)
	}

	if value1 != value2 {
		t.Fail()
	}
}

func TestDateText(t *testing.T) {
	value1 := stime.NowDate()
	data, err := value1.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var value2 stime.Date
	if err := value2.UnmarshalText(data); err != nil {
		t.Fatal(err)
	}

	if value1 != value2 {
		t.Fail()
	}
}

func equalDate(t time.Time, v stime.Date) bool {
	return t.Year() == v.Year() && t.Month() == v.Month() && t.Day() == v.Day()
}
