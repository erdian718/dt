package stime_test

import (
	"fmt"

	"gitlab.com/erdian718/stime"
)

func Example() {
	date := stime.NowDate()
	time := stime.NowTime()
	datetime := stime.NowDateTime()

	fmt.Println(date)
	fmt.Println(time)
	fmt.Println(datetime)
}
