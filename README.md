# stime

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/erdian718/stime.svg)](https://pkg.go.dev/gitlab.com/erdian718/stime)
[![Go Version](https://img.shields.io/badge/go%20version-%3E=1.20-61CFDD.svg?style=flat-square)](https://go.dev/)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/erdian718/stime)](https://goreportcard.com/report/gitlab.com/erdian718/stime)

Package stime provides some simplified (no time zone) time processing functions.

## Feature

* Conveniently convert to and from `time.Time`.
* The underlying data is of readable integer type.
* Serializing to JSON format is compatible with HTML5.

## Usage

```go
import "gitlab.com/erdian718/stime"

func main() {
	date := stime.NowDate()
	time := stime.NowTime()
	datetime := stime.NowDateTime()

	// ...
}
```

## Note

* No time zone.
