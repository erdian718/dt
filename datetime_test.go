package stime_test

import (
	"testing"
	"time"

	"gitlab.com/erdian718/stime"
)

func TestToDateTime(t *testing.T) {
	now := time.Now()
	value := stime.ToDateTime(now)
	if !equalDateTime(now, value) {
		t.Fail()
	}
}

func TestNewDateTime(t *testing.T) {
	now := time.Now()
	value := stime.NewDateTime(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second())
	if !equalDateTime(now, value) {
		t.Fail()
	}
}

func TestNowDateTime(t *testing.T) {
	now := time.Now()
	value := stime.NowDateTime()
	if !equalDateTime(now, value) {
		t.Fail()
	}
}

func TestParseDateTime(t *testing.T) {
	now := time.Now()
	value, err := stime.ParseDateTime(time.DateTime, now.Format(time.DateTime))
	if err != nil {
		t.Fatal(err)
	}
	if !equalDateTime(now, value) {
		t.Fail()
	}
}

func TestDateTimeUTC(t *testing.T) {
	now := time.Now()
	value := stime.ToDateTime(now)
	if !time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second(), 0, time.UTC).Equal(value.UTC()) {
		t.Fail()
	}
}

func TestDateTimeLocal(t *testing.T) {
	now := time.Now()
	value := stime.ToDateTime(now)
	if !time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second(), 0, time.Local).Equal(value.Local()) {
		t.Fail()
	}
}

func TestDateTimeSub(t *testing.T) {
	now := time.Now()
	dur := 24*time.Hour + time.Minute + time.Second
	value := stime.ToDateTime(now)
	if value.Add(dur).Sub(value) != dur {
		t.Fail()
	}
}

func TestDateTimeAdd(t *testing.T) {
	now := time.Now()
	dur := 24*time.Hour + time.Minute + time.Second
	value := stime.ToDateTime(now)
	if !equalDateTime(now.Add(dur), value.Add(dur)) {
		t.Fail()
	}
}

func TestDateTimeAddWith(t *testing.T) {
	now := time.Now()
	value := stime.ToDateTime(now)
	if !equalDateTime(now.AddDate(1, 1, 1).Add(time.Hour+time.Minute+time.Second), value.AddWith(1, 1, 1, 1, 1, 1)) {
		t.Fail()
	}
}

func TestDateTimeFormat(t *testing.T) {
	now := time.Now()
	value := stime.ToDateTime(now)
	if value.Format(time.DateTime) != now.Format(time.DateTime) {
		t.Fail()
	}
}

func TestDateTimeString(t *testing.T) {
	now := time.Now()
	value := stime.ToDateTime(now)
	if value.String() != now.Format(time.DateTime) {
		t.Fail()
	}
}

func TestDateTimeBinary(t *testing.T) {
	value1 := stime.NowDateTime()
	data, err := value1.MarshalBinary()
	if err != nil {
		t.Fatal(err)
	}

	var value2 stime.DateTime
	if err := value2.UnmarshalBinary(data); err != nil {
		t.Fatal(err)
	}

	if value1 != value2 {
		t.Fail()
	}
}

func TestDateTimeText(t *testing.T) {
	value1 := stime.NowDateTime()
	data, err := value1.MarshalText()
	if err != nil {
		t.Fatal(err)
	}

	var value2 stime.DateTime
	if err := value2.UnmarshalText(data); err != nil {
		t.Fatal(err)
	}

	if value1 != value2 {
		t.Fail()
	}
}

func equalDateTime(t time.Time, v stime.DateTime) bool {
	return t.Year() == v.Year() && t.Month() == v.Month() && t.Day() == v.Day() &&
		t.Hour() == v.Hour() && t.Minute() == v.Minute() && t.Second() == v.Second()
}
